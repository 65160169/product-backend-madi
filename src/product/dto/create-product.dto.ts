import {IsNotEmpty,MinLength} from 'class-validator'
export class CreateProductDto {
    id:number;

    @IsNotEmpty()
    @MinLength(5)
    name: string;

    @IsNotEmpty()
    price: number;
}
