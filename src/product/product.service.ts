import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let product:Product[] = [
  {id: 1 ,name:"Bitcion",price: 25000},
  {id: 2 ,name:"Etherium",price: 2000},
  {id: 3 ,name:"Dodge coin",price: 2},
];

let lastProductId = 4;

@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {

    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto //ตัวที่อยู่ในนี้มี name,price
    }
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if ( index < 0 ){
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if ( index < 0 ){
      throw new NotFoundException();
    }
    //console.log('product: ' + JSON.stringify(product[index]));
    //console.log('update: ' + JSON.stringify(updateProductDto));
    const updateProduct:Product = { 
      ...product[index],
      ...updateProductDto
    }
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if ( index < 0 ){
      throw new NotFoundException();
    }
    const deletedProduct = product[index];
    product.splice(index,1);
    return deletedProduct;
    return `This action removes a #${id} product`;
  }

  reset() {
    product = [
      {id: 1 ,name:"Bitcion",price: 25000},
      {id: 2 ,name:"Etherium",price: 2000},
      {id: 3 ,name:"Dodge coin",price: 2},
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
